{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "$id": "https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/config/custom_abilities/type_schema.json",
  "type": "object",
  "additionalProperties": false,
  "properties": {
    "title": {
      "type": "string",
      "description": "Human readable title for the custom ability"
    },
    "name": {
      "type": "string",
      "description": "Unique identifying name for this custom ability"
    },
    "description": {
      "type": "string",
      "description": "A human-readable description of this custom ability"
    },
    "introduced_by_issue": {
      "type": "string",
      "format": "uri",
      "description": "URL to GitLab issue that added this custom ability",
      "qt-uri-protocols": [
        "https"
      ]
    },
    "introduced_by_mr": {
      "type": "string",
      "format": "uri",
      "description": "URL to GitLab merge request that added this custom ability",
      "qt-uri-protocols": [
        "https"
      ]
    },
    "feature_category": {
      "type": "string",
      "description": "The feature category of this this custom ability. For example, vulnerability_management"
    },
    "milestone": {
      "type": "string",
      "description": "Milestone that introduced this custom ability. For example, 15.8",
      "pattern": "^[0-9]+\\.[0-9]+$"
    },
    "admin_ability": {
      "type": "boolean",
      "description": "Indicate whether this ability is checked at the admin level."
    },
    "group_ability": {
      "type": "boolean",
      "description": "Indicate whether this ability is checked on group level."
    },
    "enabled_for_group_access_levels": {
      "type": "array",
      "description": "Specifies group access levels where the custom ability is enabled by default",
      "items": {
        "enum": [
          10,
          15,
          20,
          30,
          40,
          50
        ]
      }
    },
    "project_ability": {
      "type": "boolean",
      "description": "Indicate whether this ability is checked on project level."
    },
    "enabled_for_project_access_levels": {
      "type": "array",
      "description": "Specifies project access levels where the custom ability is enabled by default",
      "items": {
        "enum": [
          10,
          15,
          20,
          30,
          40,
          50
        ]
      }
    },
    "requirements": {
      "type": "array",
      "description": "The custom abilities that need to be enabled for this ability."
    },
    "skip_seat_consumption": {
      "type": "boolean",
      "description": "Indicate wheter this ability should be skiped when counting licensed users"
    },
    "available_from_access_level": {
      "type": "integer",
      "description": "The access level from which this ability is available.",
      "enum": [
        10,
        15,
        20,
        30,
        40,
        50,
        60
      ]
    }
  },
  "allOf": [
    {
      "if": {
        "properties": {
          "project_ability": {
            "const": true
          }
        }
      },
      "then": {
        "required": [
          "enabled_for_project_access_levels"
        ]
      }
    },
    {
      "if": {
        "properties": {
          "group_ability": {
            "const": true
          }
        }
      },
      "then": {
        "required": [
          "enabled_for_group_access_levels"
        ]
      }
    }
  ],
  "required": [
    "description",
    "feature_category",
    "group_ability",
    "introduced_by_issue",
    "introduced_by_mr",
    "milestone",
    "name",
    "project_ability"
  ],
  "not": {
    "properties": {
      "project_ability": {
        "enum": [
          false
        ]
      },
      "group_ability": {
        "enum": [
          false
        ]
      },
      "admin_ability": {
        "enum": [
          false
        ]
      }
    }
  },
  "title": "GitLabCustomPermission"
}
